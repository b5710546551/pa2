package ku.util;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * ArrayIterator can get the elements in array(from constructor), 
 * check whether its has any others and remove those one by one.
 * 
 * @author Chinatip Vichian
 *
 * @param <T> is input data type.
 */
public class ArrayIterator<T> implements Iterator<T>{
	
	/** Array of clone data of array that was received from constructor */
	private T[] array;
	
	/**
	 * Constructor of ArrayIterator
	 * @param arr is array for construct ArrayIterator
	 */
	public ArrayIterator (T[] arr){
		this.array = arr.clone();
	}
	
	/**
	 * Check that is there any elements in ArrayIterator
	 * @return a.) true, if ArrayIterator still have other elements in array left.
	 * b.) false, if ArrayIterator is null.
	 */
	public boolean hasNext() {
		if(array.length<=1||array==null)
			return false;
		return true;
	}

	/**
	 * Get a next element of array
	 * @return a next element of array 
	 */
	public T next() {
		if(array.length==0){
			throw new NoSuchElementException( );
		}
		T temp = array[0];
		if(array.length==1){
			remove();
			return temp;
		}
		this.remove();

		if(temp==null)
			return next();
		return temp;
	}

	/**
	 * Remove most a recent element
	 */
	public void remove(){
		if(array.length==1){
			array = (T[]) new Object[0];
		}
		if(array.length>1)
			array = (T[]) Arrays.copyOfRange(array,1,array.length);
	}
}
