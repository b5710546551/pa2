package ku.util;

import java.util.Arrays;
import java.util.EmptyStackException;

/**
 * Stack is able to put(push method) things in stack(not more than its capacity).
 * It can be peek to know what it is before pop it out.
 * 
 * @author Chinatip Vichian
 *
 * @param <T> is input data type.
 */
public class Stack<T> {
	
	/** Array saved object will be peeked, pushed and popped*/
	private T[] items;
	
	/** Number used to run index of items array */
	private int index;
	
/**
 * Constructor of Stack class
 * @param capacity is the size of the items array.
 */
	public Stack(int capacity){
		if(capacity<0)items = (T[]) new Object[0];
		else items = (T[]) new Object[capacity];
		index = 0;
	}

	/**
	 * Get capacity of Stack.
	 * @return a.) -1, if items array of Stack is unknown or infinite.
	 * b.) capacity is the maximum number of elements that this Stack can hold. ( Capacity must be a positive integer.)
	 */
	public int capacity(){
		if (items==null) return -1;
		return items.length;
	}
	
	/**
	 * Check that Stack is empty or not
	 * @return a.) true, if Stack is empty
	 *         b.) false, if Stack is not empty.
	 */
	public boolean isEmpty(){
		return index==0||items[0]==null;
	}

	/**
	 * Check whether the Stack is full or not
	 * @return a.) true, if Stack is full
	 *		   b.) false, if it is not.
	 */
	public boolean isFull(){
		return (capacity()==size());
	}

	/**
	 * Get the item on the top of the stack
	 * @return a.) null,if Stack is empty. 
	 *         b.) The item on the top of the stack.
	 */
	public T peek(){
		if(isEmpty())
			return null;
		if(index==1)
			return items[0];
		return items[index-1];
	}
	
	/**
	 * Get and remove the item on the top of the stack
	 * @return a.) Throw EmptyStackException,if Stack is empty. 
	 *         b.) The item on the top of the stack.
	 */
	public T pop (){
		if(isEmpty())
			throw new EmptyStackException();

		if(index>1){
			T temp = items[index-1];
			items = Arrays.copyOfRange(items, 0, index-1);
			index--;
			return temp;
		}
		else {
			T temp = items[0];
			items = (T[]) new Object[1];
			index = 0;
			return temp;
		}

	}

	/**
	 * Push a new item onto the top of the stack
	 * @param obj is a new object will be push
	 */
	public void push(T obj){
		if(obj == null)
			throw new IllegalArgumentException();
		if(!isFull()){
			items[index] = obj;
			index++;
		}
	}

	/**
	 * Get the number of items in the stack
	 * @return The number of items in the stack
	 */
	public int size(){
		if(isEmpty())
			return 0;
		return index;
	}
}
